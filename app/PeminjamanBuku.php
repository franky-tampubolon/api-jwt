<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PeminjamanBuku extends Model
{
    protected $guarded = ['id'];
    protected $table = 'data_peminjaman_buku';
}

<?php

namespace App\Http\Controllers;

use App\Buku;
use Carbon\Traits\Timestamp;
use Illuminate\Http\Request;

class BukuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $buku = Buku::all();

        return $buku;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request);
        $request->validate([
            // 'kode_buku' => 'required|unique:buku,kode_buku',
            'judul'     => 'required',
            'pengarang' => 'required',
            'tahun_terbit'  => 'required'
        ]);

        // generate kode buku
        $thn = substr($request->tahun_terbit, 0, 2);
        $judul = substr($request->judul, 0, 3);
        $pengarang = substr($request->pengarang, 0, 5);
        $random = mt_rand();

        Buku::create([
            'kode_buku' => $thn .'-'.$judul .'-' .$pengarang .'-'.$random,
            'judul' => $request->judul,
            'pengarang' => $request->pengarang,
            'tahun_terbit' => $request->tahun_terbit,

        ]);

        return response('Data buku berhasil ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Buku  $buku
     * @return \Illuminate\Http\Response
     */
    public function show(Buku $buku)
    {
        // 
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Buku  $buku
     * @return \Illuminate\Http\Response
     */
    public function edit(Buku $buku)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Buku  $buku
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Buku $buku)
    {
        // dd($buku->id);
        $request->validate([
            // 'kode_buku' => 'required|unique:buku,kode_buku',
            'judul'     => 'required',
            'pengarang' => 'required',
            'tahun_terbit'  => 'required'
        ]);

        $data = [
            'judul' => $request->judul,
            'pengarang' => $request->pengarang,
            'tahun_terbit' => $request->tahun_terbit,

        ];
        // dd($data);

        Buku::where('id', $buku->id)->update($data);

        return response('Data buku berhasil diupdate');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Buku  $buku
     * @return \Illuminate\Http\Response
     */
    public function destroy(Buku $buku)
    {
        //
    }
}

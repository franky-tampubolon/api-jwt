<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $data = [
            'name'  => $request->nama,
            'email'  => $request->email,
            'password'  => Hash::make($request->password),
        ];
        Validator::make($data, [
            'nama'  =>  'required|string',
            'email'  =>  'required|email|unique:users,email',
            'password'  =>  'required|min;8',
        ]);

        $user = User::create($data);

        $user->roles()->attach(1);

        return response('register berhasil');
    }
}

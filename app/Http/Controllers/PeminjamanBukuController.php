<?php

namespace App\Http\Controllers;

use App\PeminjamanBuku;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PeminjamanBukuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // $request->validate([
        //     'tanggal_peminjaman'    =>'required|date'
        // ]);
            // dd($request->tanggal_peminjaman);
        $a = Carbon::parse($request->tanggal_peminjaman);
        // return $a->addDays(7);

        PeminjamanBuku::create([
            'buku_id'   => $request->buku_id,
            'mahasiswa_id'  => Auth::user()->id,
            'tanggal_peminjaman'    =>$request->tanggal_peminjaman,
            'batas_akhir_peminjaman'    => $a->addDays(7),
            'status_ontime'     => 0
        ]);

        return response('Peminjaman buku berhasil');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PeminjamanBuku  $peminjamanBuku
     * @return \Illuminate\Http\Response
     */
    public function show(PeminjamanBuku $peminjamanBuku)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PeminjamanBuku  $peminjamanBuku
     * @return \Illuminate\Http\Response
     */
    public function edit(PeminjamanBuku $peminjamanBuku)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PeminjamanBuku  $peminjamanBuku
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $data_pinjam = PeminjamanBuku::where([
            ['buku_id', 1],
            ['mahasiswa_id', 1]
        ])->first()->batas_akhir_peminjaman;

        $tgl_kembali = Carbon::now();
        $status = 0;
        if($tgl_kembali <= $data_pinjam){
            $status = 1;
        }
        else{
            $status = 2;
        }

        dd("Buku id adalah: " .$request->buku_id);

        PeminjamanBuku::where([
            ['buku_id', $request->buku_id],
            ['mahasiswa_id', $request->mahasiswa_id]
        ])->update([
            'status_ontime' => $status,
            'tanggal_pengembalian'  => $tgl_kembali
        ]);

        return response('Buku telah dikembalikan');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PeminjamanBuku  $peminjamanBuku
     * @return \Illuminate\Http\Response
     */
    public function destroy(PeminjamanBuku $peminjamanBuku)
    {
        //
    }
}

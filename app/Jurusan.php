<?php

namespace App;

use App\Fakultas;
use Illuminate\Database\Eloquent\Model;

class Jurusan extends Model
{
    protected $guarded = ['id'];

    protected $table = 'jurusan';

    public function fakultas()
    {
        return $this->belongsTo(Fakultas::class);
    }
}

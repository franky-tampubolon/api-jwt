<?php

namespace App;

use App\Jurusan;
use Illuminate\Database\Eloquent\Model;

class Fakultas extends Model
{
    protected $guarded = ['id'];

    protected $table = 'fakultas';

    public function jurusan()
    {
        return $this->hasMany(Jurusan::class);
    }
}

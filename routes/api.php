<?php

Route::namespace('Auth')->group(function(){
    Route::post('/register', 'RegisterController');
    Route::post('/login', 'LoginController');
    Route::post('/logout', 'LogoutController');
});


Route::middleware(['auth', 'roleMiddleware'])->group(function() {
    Route::get('/home', 'HomeController@index');    
    Route::post('/fakultas', 'FakultasController@store');
    Route::post('/jurusan', 'JurusanController@store');
    Route::post('/buku', 'BukuController@store');
    Route::put('/buku/{buku}', 'BukuController@update');
    Route::post('/pinjambuku', 'PeminjamanBukuController@store');
    Route::put('/kembalikanbuku', 'PeminjamanBukuController@update');
});

Route::post('/mahasiswa', 'MahasiswaController@store')->middleware('auth');
Route::get('/mahasiswa', 'MahasiswaController@index')->middleware('auth');